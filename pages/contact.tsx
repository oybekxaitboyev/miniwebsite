import React from "react";
import { useState } from "react";


const ContactPage = () => {
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [text, setText] = useState("");
  let url = `https://api.telegram.org/bot6004747091:AAEPHQcyYt_zvc2OBkSJ6UqBCiTCEhk7ZQQ/sendMessage?chat_id=1197709139&text=%0A-Isim: ${name}, %0A- Raqam: ${phone},  %0A- Matin: ${text}, %0A`;
  function sendMassageFunction() {
    fetch(url).then((res) => res.json());
  }

  return (
   
      <div className={"contact"} data-aos="flip-up" data-aos-duration="1500">
       
        <div className="arrow"></div>
        <form>
          <input
            defaultValue={name}
            onChange={(e) => setName(e.target.value)}
            id="name"
            type="text"
            placeholder={"Ismingiz"}
          />

          <input
            defaultValue={phone}
            onChange={(e) => setPhone(e.target.value)}
            id={"phone"}
            type="tel"
            placeholder={"Telefon raqamingiz"}
          />

      
          <button
            className="aloqabtn"
            onClick={sendMassageFunction}
            type="submit"
          >
            Xabarni yuborish
          </button>
        </form>
        
     </div>
 
  );

};





export default ContactPage;
