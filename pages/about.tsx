import React from "react";
const AboutPage = () => {
  return (
    <div>
      About Page <br /> <br /> Lorem ipsum dolor sit amet consectetur,
      adipisicing elit. Quam eligendi harum magnam odio dignissimos error,
      nesciunt voluptatem! Neque quia porro soluta nam quis nesciunt blanditiis
      itaque deleniti eos! Enim odio ut alias, quo unde exercitationem eum vero
      magni mollitia perspiciatis, deserunt officia maiores minima nam a, minus
      odit commodi beatae distinctio atque amet. Voluptatum aperiam temporibus
      quod ipsa magnam et quam soluta vitae eaque ipsum rerum molestias
      repellendus minus nesciunt, hic in qui. Alias doloremque cum iste.
      Voluptatibus unde maxime nobis optio, quasi eos incidunt veritatis,
      perferendis officiis tenetur ad numquam quod aperiam, velit
      necessitatibus! Cupiditate, doloremque! Reiciendis debitis dolore maxime
      tenetur hic voluptatem totam veritatis distinctio odit! Asperiores
      architecto quod suscipit ipsam dolore iure voluptatibus cumque laborum
      corrupti nisi quo aperiam quos dolor laudantium, nobis, sint voluptatem.
      Ad aliquid ut nostrum facere? Ab veritatis perspiciatis ex deleniti
      eligendi nesciunt ea illo assumenda! Iste iure quidem voluptate doloribus
      in, vitae explicabo consequatur expedita quibusdam dignissimos suscipit id
      veritatis at error? Doloribus quia, minima laboriosam accusamus dolores
      odit et accusantium iste ex quae. Nemo inventore nihil, eos molestias
      natus dolor et ipsum, iste enim quo dicta praesentium omnis accusantium
      repellendus saepe accusamus nostrum reiciendis minima placeat magni unde
      laboriosam, tempore nam?
    </div>
  );
};
export default AboutPage;
